FROM nginx:1.17.0

RUN rm -rvf /etc/nginx/conf.d/*
RUN touch /etc/nginx/conf.d/_http.conf \
    && touch /etc/nginx/conf.d/_server.conf
COPY nginx.conf /etc/nginx/nginx.conf
